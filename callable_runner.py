__author__ = 'Jonathan'

class CallableRunner:
    def __init__(self):
        pass

    def run(self):
        self.execute_test_module('testcase_class','TestcaseClass')

    def execute_test_module(self, module_name, class_name):
        message =  "Reading Test ID  1"
        m =__import__(module_name,globals(), locals(), class_name)
        c = getattr(m, class_name)
        c().sampleTest()
