__author__ = 'Jonathan'

import logging, time, sys

class LoggerClass:
    default = "__app__"
    formatter = logging.Formatter('%(levelname)s: %(asctime)s %(funcName)s(%(lineno)d) -- %(message)s',
                              datefmt='%Y-%m-%d %H:%M:%S')
    FORMAT = '%(asctime)s - %(name)s - [ %(levelname)s ] - %(message)s'
    logger = logging.Logger
    handlerList = []
    fileHandler = logging.FileHandler
    consoleHandler= logging.StreamHandler
    logfile = 'report/'+time.strftime("%Y%m%d_%H%M%S")+'_sample.log'

    def __init__(self):
        pass


    def initiate_logging_service(self, name=None):
        #self.logger.setLevel(logging.INFO)
        self.fileHandler = logging.FileHandler(self.logfile)
        self.fileHandler.setFormatter(self.FORMAT)
        self.logger.addHandler(self.fileHandler)
        self.handlerList.append(self.fileHandler)

        self.consoleHandler = logging.StreamHandler(sys.stdout)
        self.consoleHandler.setFormatter(self.FORMAT)
        self.logger.addHandler(self.consoleHandler)
        self.handlerList.append(self.consoleHandler)
        self.logger.info("loggerclass")


    def get_logger(self, name=None):
        default = "___name___"
        formatter = logging.Formatter('%(asctime)s : %(module)s.%(funcName)s [%(levelname)s] - %(message)s',
                              datefmt='%Y-%m-%d %H:%M:%S')
        if name:
            logger = logging.getLogger(name)
        else:
            logger = logging.getLogger(default)
        fh = logging.FileHandler(self.logfile)
        fh.setFormatter(formatter)
        logger.addHandler(fh)
        logger.setLevel(logging.DEBUG)



        self.consoleHandler = logging.StreamHandler(sys.stdout)
        self.consoleHandler.setFormatter(formatter)
        logger.addHandler(self.consoleHandler)
        self.handlerList.append(self.consoleHandler)


        return logger



    def shutdown_logging_service(self):
        self.fileHandler.flush()
        self.consoleHandler.flush()
        self.consoleHandler.close()
        self.fileHandler.close()


