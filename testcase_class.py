__author__ = 'Jonathan'
import logging
from logger_class import LoggerClass


class TestcaseClass:
     logger = logging.Logger
     lc = LoggerClass()

     def __init__(self):
        self.logger = self.lc.get_logger('___name___')

     def sampleTest(self):
         self.logger.info("Sample Test info")
         self.logger.debug("Sample Test debug")
         self.logger.warn("Sample Test warn")
         self.logger.error("Sample Test error")
